/**
 * Created by timur on 14.03.17.
 */
import React from 'react';
import PokemonInfo from  './PokemonInfo';
import  { Button, Modal } from 'react-bootstrap/lib';

const PokeModal = ({showModal, closeModal, pokemon}) => {
  return (
    <div>
        <Modal
            show={showModal}
            onHide={closeModal}
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title">   { pokemon !== null  ? pokemon.name : "Loading..."}  </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                { pokemon !== null ?
                    <PokemonInfo pokemon={pokemon} /> :
                        null  }
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={closeModal}>Close</Button>
            </Modal.Footer>
        </Modal>
    </div>
  );
};

export default PokeModal;